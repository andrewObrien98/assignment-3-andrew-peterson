/* TODO: Build up the HTML document by using JavaScript DOM manipulation functions.
 * Do not rely on changes you may have made to index.html because the grader won't use that file.
 */
document.title = "Interactive JavaScript Calculator"

//create heading and prompt
var mainDiv = document.createElement("div");
mainDiv.setAttribute("class", "black");
document.body.appendChild(mainDiv);
var heading = document.createElement("h1");
heading.setAttribute("class", "stuff-box");
heading.textContent = "JavaScript Calculator";
var propmt = document.createElement("h3");
propmt.setAttribute("class", "stuff-box");
propmt.textContent = "Create An Expression:";
mainDiv.appendChild(heading);
mainDiv.appendChild(propmt);


//create the form
var form = document.createElement("form");
form.setAttribute("class", "stuff-box");
mainDiv.appendChild(form);

//create input 1
var input1 = document.createElement("input");
input1.setAttribute("type", "number");
input1.setAttribute("placeholder", "0");
input1.setAttribute("name", "input1");
form.appendChild(input1);

//create the drop down menu
var menu = document.createElement("select");
menu.setAttribute("name", "menu");
menu.setAttribute("id", "menu");
form.appendChild(menu);


//add operators to the menu
var operators = ["+", "-", "*", "/","%", "**"];
for(let operator of operators){
    var option = document.createElement("option")
    option.textContent = operator;
    menu.appendChild(option);
};

//create input 2
var input2 = document.createElement("input");
input2.setAttribute("type", "number");
input2.setAttribute("placeholder", "0");
input2.setAttribute("name", "input1");
form.appendChild(input2);

//create Button
var button = document.createElement("button");
button.setAttribute("type", "button")
button.textContent = "Compute";
form.appendChild(button);

//create color scheme
var p = document.createElement("p");
form.appendChild(p);
var color = document.createElement("input");
color.setAttribute("type", "color");
color.setAttribute("name", "color");
var colorLabel = document.createElement("label");
colorLabel.setAttribute("for", "color");
colorLabel.textContent = " Color of new result <Div>";
p.appendChild(color);
p.appendChild(colorLabel);


//event listeners


var value = 0;
var date = "";
button.addEventListener("click", function(){
    //make sure it works?
    setDate();
    if(input1.value === "" || input2.value ===""){
        addErrorDiv();
    } else {
        value = setValue();
        addNewDiv();
    }  
});

//this will set the value for the computed function
var setValue = function(){
    let num1 = eval(input1.value);
    let num2 = eval(input2.value);
    if(menu.value === "+"){
        return num1 + num2;
    }
        else if(menu.value === "-"){
        return num1 - num2;
    }
        else if(menu.value === "*"){
        return num1 * num2;
    }
        else if(menu.value === "/"){
        return num1 / num2;
    }
        else if(menu.value === "%"){
        return num1 % num2;
    }
        else if(menu.value === "**"){
        return num1 ** num2;
    };
    return 0;
};

//this will add the new div with all its elements
var addNewDiv = function(){
    var newDiv = document.createElement("div");
    //make sure to add the date as well
    var textValue = input1.value + " " + menu.value + " " + input2.value + " = ";
    var dateP = document.createElement('text');
    dateP.setAttribute("class", "timestamp")
    var opP = document.createElement('text');
    dateP.textContent = "Date created: " + date + "  :  ";
    opP.textContent = "Operation conducted: " + textValue + value;
    newDiv.appendChild(dateP);
    newDiv.appendChild(opP);
    


    newDiv.setAttribute("class", "stuff-box");
    var newColor = "background-color: " + color.value;
    newDiv.setAttribute("style", newColor);
    var neighbor = mainDiv.nextElementSibling;
    document.body.insertBefore(newDiv, neighbor);

    newDiv.addEventListener("click", function(){
        newDiv.remove();
    });
};

//this will add an error div with the date and error message
var addErrorDiv = function() {
    var newDiv = document.createElement("div");
    //make sure to add the date as well
    var dateP = document.createElement('text');
    dateP.setAttribute("class", "timestamp")
    var opP = document.createElement('text');
    dateP.textContent = "Date created: " + date + "  :  ";
    opP.textContent = "ERROR: Missing One Or More Operands!";
    newDiv.appendChild(dateP);
    newDiv.appendChild(opP);
    //newDiv.textContent = date + "   ERROR: Missing One Or More Operands!";


    newDiv.setAttribute("class", "stuff-box");
    var newColor = "background-color: darkred";
    newDiv.setAttribute("style", newColor);
    var neighbor = mainDiv.nextElementSibling;
    document.body.insertBefore(newDiv, neighbor);

    newDiv.addEventListener("click", function(){
        newDiv.remove();
    });
};

//this will set the variable date = to the current date
var setDate = function(){
    date = new Date().toString();
};
